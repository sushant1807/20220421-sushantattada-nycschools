package com.saika.nycschools.repositories.schools;

import com.saika.nycschools.Models.SchoolModel;
import com.saika.nycschools.Models.SchoolSATScores;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Query;

public interface SchoolsDataSource {

    Single<List<SchoolModel>> getSchoolData(int pageSize);

    Single<List<SchoolSATScores>> getSchoolSatScore(String dbn);
}
