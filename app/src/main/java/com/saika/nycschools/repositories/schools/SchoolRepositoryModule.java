package com.saika.nycschools.repositories.schools;

import com.saika.nycschools.repositories.Remote;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SchoolRepositoryModule {

    @Provides
    @Remote
    @Singleton
    public SchoolsDataSource provideRemoteDataSource(SchoolsRemoteDataSource schoolsRemoteDataSource) {
        return schoolsRemoteDataSource;
    }

    //This commented code is for local data source which is used for storing data into the local

//    @Provides
//    @Remote
//    @Singleton
//    public SchoolsDataSource provideLocalDataSource(SchoolsLocalDataSource schoolsLocalDataSource) {
//        return schoolsLocalDataSource;
//    }
}
