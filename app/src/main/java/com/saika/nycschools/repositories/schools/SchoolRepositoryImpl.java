package com.saika.nycschools.repositories.schools;

import androidx.annotation.VisibleForTesting;

import com.saika.nycschools.Models.SchoolModel;
import com.saika.nycschools.Models.SchoolSATScores;
import com.saika.nycschools.repositories.Remote;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class SchoolRepositoryImpl implements SchoolsDataSource {

    @VisibleForTesting
    private final SchoolsDataSource remoteDataSource;

    @Inject
    public SchoolRepositoryImpl(@Remote SchoolsDataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }


    @Override
    public Single<List<SchoolModel>> getSchoolData(int pageSize) {
        return remoteDataSource.getSchoolData(pageSize);
    }

    @Override
    public Single<List<SchoolSATScores>> getSchoolSatScore(String dbn) {
        return remoteDataSource.getSchoolSatScore(dbn);
    }


}
