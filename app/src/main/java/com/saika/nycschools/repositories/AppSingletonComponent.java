package com.saika.nycschools.repositories;

import com.saika.nycschools.data.ApiServiceModule;
import com.saika.nycschools.data.AppModule;
import com.saika.nycschools.repositories.schools.SchoolRepositoryImpl;
import com.saika.nycschools.repositories.schools.SchoolRepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {SchoolRepositoryModule.class, AppModule.class, ApiServiceModule.class })
public interface AppSingletonComponent {

    SchoolRepositoryImpl provideSchoolRepositoryImpl();
}
