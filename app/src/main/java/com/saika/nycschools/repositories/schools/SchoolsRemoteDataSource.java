package com.saika.nycschools.repositories.schools;

import com.saika.nycschools.Models.SchoolModel;
import com.saika.nycschools.Models.SchoolSATScores;
import com.saika.nycschools.services.SchoolService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class SchoolsRemoteDataSource implements SchoolsDataSource{

    private final SchoolService schoolService;

    @Inject
    public SchoolsRemoteDataSource(SchoolService schoolService){
        this.schoolService = schoolService;
    }

    @Override
    public Single<List<SchoolModel>> getSchoolData(int pageSize) {
        return schoolService.getSchoolData(pageSize);
    }

    @Override
    public Single<List<SchoolSATScores>> getSchoolSatScore(String dbn) {
        return schoolService.getSchoolSatScore(dbn);
    }
}
