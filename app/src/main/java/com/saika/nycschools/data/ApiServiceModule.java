package com.saika.nycschools.data;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.saika.nycschools.Utils.Constants;
import com.saika.nycschools.services.SchoolService;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiServiceModule {

    private static final String BASE_URL = "base_url";

    public ApiServiceModule() {
        Log.i(getClass().getSimpleName(), "constructor hash=0x" + Integer.toHexString(hashCode()));
    }

    @Provides
    @Named(BASE_URL)
    String provideBaseUrl() {
        return Constants.API_URL;
    }

    @Provides
    @Singleton
    OkHttpClient provideHttpClient() {

        return new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Converter.Factory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    CallAdapter.Factory provideRxJavaAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(@Named(BASE_URL) String baseUrl, Converter.Factory converterFactory,
                             CallAdapter.Factory callAdapterFactory, OkHttpClient client) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        //To check the commented code while converting in to GSON
        return new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    SchoolService provideSchoolService(Retrofit retrofit) {
        return retrofit.create(SchoolService.class);
    }
}
