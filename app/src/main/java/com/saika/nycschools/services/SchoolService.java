package com.saika.nycschools.services;

import com.saika.nycschools.Models.SchoolModel;
import com.saika.nycschools.Models.SchoolSATScores;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolService {

    @GET("resource/s3k6-pzi2.json")
    Single<List<SchoolModel>> getSchoolData(@Query("$limit") int pageSize);

    @GET("resource/f9bf-2cp4.json")
    Single<List<SchoolSATScores>> getSchoolSatScore(@Query("dbn") String dbn);
}
