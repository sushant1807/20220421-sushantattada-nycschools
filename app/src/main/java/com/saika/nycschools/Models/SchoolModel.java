package com.saika.nycschools.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SchoolModel implements Parcelable, Serializable {

    @SerializedName("dbn")
    public String dbn;
    @SerializedName("school_name")
    public String schoolName;
    @SerializedName("overview_paragraph")
    public String overviewParagraph;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("school_email")
    private String schoolEmail;
    @SerializedName("website")
    private String website;
    @SerializedName("primary_address_line_1")
    private String primaryAddressLine1;
    @SerializedName("city")
    private String city;
    @SerializedName("zip")
    private String zip;
    @SerializedName("state_code")
    private String stateCode;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;

    public SchoolModel() {
    }

    protected SchoolModel(Parcel in) {
        dbn = in.readString();
        schoolName = in.readString();
        overviewParagraph = in.readString();
        phoneNumber = in.readString();
        schoolEmail = in.readString();
        website = in.readString();
        primaryAddressLine1 = in.readString();
        city = in.readString();
        zip = in.readString();
        stateCode = in.readString();
        latitude = in.readString();
        longitude = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dbn);
        dest.writeString(schoolName);
        dest.writeString(overviewParagraph);
        dest.writeString(phoneNumber);
        dest.writeString(schoolEmail);
        dest.writeString(website);
        dest.writeString(primaryAddressLine1);
        dest.writeString(city);
        dest.writeString(zip);
        dest.writeString(stateCode);
        dest.writeString(latitude);
        dest.writeString(longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SchoolModel> CREATOR = new Creator<SchoolModel>() {
        @Override
        public SchoolModel createFromParcel(Parcel in) {
            return new SchoolModel(in);
        }

        @Override
        public SchoolModel[] newArray(int size) {
            return new SchoolModel[size];
        }
    };

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPrimaryAddressLine1() {
        return primaryAddressLine1;
    }

    public void setPrimaryAddressLine1(String primaryAddressLine1) {
        this.primaryAddressLine1 = primaryAddressLine1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "{" +
                "dbn='" + dbn + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", overviewParagraph='" + overviewParagraph + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", schoolEmail='" + schoolEmail + '\'' +
                ", website='" + website + '\'' +
                ", primaryAddressLine1='" + primaryAddressLine1 + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                ", stateCode='" + stateCode + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
