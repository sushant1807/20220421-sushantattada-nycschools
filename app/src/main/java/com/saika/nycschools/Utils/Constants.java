package com.saika.nycschools.Utils;

public class Constants {

    public static final String API_URL = "https://data.cityofnewyork.us/";
    public static final int PAGE_SIZE = 100;
    public static final String DBN = "dbn";
    public static final String SCHOOL_NAME = "school_name";
    public static final String PARAGRAPH = "paragraph";
}
