package com.saika.nycschools;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.saika.nycschools.data.AppModule;
import com.saika.nycschools.repositories.AppSingletonComponent;
import com.saika.nycschools.repositories.DaggerAppSingletonComponent;

public class AndroidApplication extends Application implements Application.ActivityLifecycleCallbacks{

    private static AndroidApplication context;
    private AppSingletonComponent appSingletonComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
        registerActivityLifecycleCallbacks(this);
        initializeDependencies();
    }

    public static AndroidApplication getContext(){
        return context;
    }

    private void initializeDependencies() {

        AppModule appModule = new AppModule(this);
        appSingletonComponent = DaggerAppSingletonComponent.builder()
                .appModule(appModule)
                .build();

    }

    public AppSingletonComponent getAppSingletonComponent() {
        return appSingletonComponent;
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }
}
