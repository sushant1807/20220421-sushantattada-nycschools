package com.saika.nycschools.ui.school;

import com.saika.nycschools.ActivityScope;
import com.saika.nycschools.repositories.AppSingletonComponent;

import dagger.Component;

@ActivityScope
@Component(modules = {SchoolPresenterModule.class}, dependencies = {AppSingletonComponent.class})
public interface SchoolComponent {
    void inject(SchoolActivity schoolActivity);
}
