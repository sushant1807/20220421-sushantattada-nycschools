package com.saika.nycschools.ui.school;

import com.saika.nycschools.Models.SchoolModel;

import java.util.List;

public interface SchoolContract {

    interface View {

        void displaySchoolDataToView(List<SchoolModel> schoolModels);

        void handleApiFailure();
    }

    interface Presenter {
        void getSchoolData();
    }
}
