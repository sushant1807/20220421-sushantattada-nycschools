package com.saika.nycschools.ui.school;

import android.content.Context;
import android.util.Log;

import com.saika.nycschools.Utils.Constants;
import com.saika.nycschools.repositories.schools.SchoolRepositoryImpl;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SchoolPresenter implements SchoolContract.Presenter{

    private final WeakReference<SchoolContract.View> view;
    private final CompositeDisposable compositeDisposable;
    private final Context context;
    private final SchoolRepositoryImpl schoolRepository;

    @Inject
    public SchoolPresenter(SchoolRepositoryImpl schoolRepository, SchoolContract.View view,
                           Context context){
        this.schoolRepository = schoolRepository;
        this.view = new WeakReference<>(view);
        this.context = context;
        this.compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void getSchoolData() {
        Disposable disposable = schoolRepository.getSchoolData(Constants.PAGE_SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(schoolModels -> {
                    view.get().displaySchoolDataToView(schoolModels);
                    Log.e("Error","Success at getSchoolData "+schoolModels.size());
                }, throwable -> {
                    view.get().handleApiFailure();
                    Log.e("Error","Failure at getSchoolData "+throwable.getMessage());
                });

        compositeDisposable.add(disposable);
    }
}
