package com.saika.nycschools.ui.school;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class SchoolPresenterModule {

    private SchoolContract.View view;
    private Context context;

    public SchoolPresenterModule(SchoolContract.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Provides
    public SchoolContract.View provideView() {
        return view;
    }

    @Provides
    public Context provideContext() {
        return context;
    }
}
