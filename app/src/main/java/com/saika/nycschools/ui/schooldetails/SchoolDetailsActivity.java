package com.saika.nycschools.ui.schooldetails;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.saika.nycschools.AndroidApplication;
import com.saika.nycschools.Models.SchoolSATScores;
import com.saika.nycschools.R;
import com.saika.nycschools.Utils.Constants;
import com.saika.nycschools.repositories.AppSingletonComponent;
import com.saika.nycschools.ui.school.SchoolActivity;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SchoolDetailsActivity extends AppCompatActivity {

    private String dbnValue;
    private CompositeDisposable compositeDisposable;
    private TextView schoolNameTv;
    private TextView satTestTakers;
    private TextView satCriticalAvgScore;
    private TextView satMatAvg;
    private TextView satWritingAvg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_details);

        schoolNameTv = findViewById(R.id.textview_school_name);
        satTestTakers = findViewById(R.id.num_of_sat_test_takers);
        satCriticalAvgScore = findViewById(R.id.sat_critical_reading_avg_score);
        satMatAvg = findViewById(R.id.sat_math_avg_score);
        satWritingAvg = findViewById(R.id.sat_writing_avg_score);

        Bundle bundle = getIntent().getExtras();
        dbnValue = bundle.getString(Constants.DBN);

        compositeDisposable = new CompositeDisposable();

        getSchoolSatScores();

    }

    protected AppSingletonComponent getAppSingletonComponent() {
        return ((AndroidApplication) getApplication()).getAppSingletonComponent();
    }

    private void getSchoolSatScores() {
        Disposable disposable = getAppSingletonComponent().provideSchoolRepositoryImpl().getSchoolSatScore(dbnValue)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(schoolSATScores -> {
                    Log.e("Error","Success at getSchoolSatScores "+schoolSATScores.size());
                    if(schoolSATScores.size() != 0) {
                        updateUISegment(schoolSATScores);
                    } else {
                        //Display toast message
                        navigateToSchoolActivity();
                        Toast.makeText(getApplicationContext(), R.string.toast_display_message, Toast.LENGTH_LONG).show();
                    }
                }, throwable -> {
                    Log.e("Error","Failure at getSchoolSatScores "+throwable.getMessage());
                    navigateToSchoolActivity();
                    Toast.makeText(getApplicationContext(), R.string.failure_school_message, Toast.LENGTH_LONG).show();
                });

        compositeDisposable.add(disposable);

    }

    private void navigateToSchoolActivity(){
        Intent intent = new Intent(SchoolDetailsActivity.this, SchoolActivity.class);
        startActivity(intent);
        finish();
    }

    private void updateUISegment(List<SchoolSATScores> schoolSATScores) {
        schoolNameTv.setText(schoolSATScores.get(0).getSchoolName());
        satTestTakers.setText(schoolSATScores.get(0).getNumOfSatTestTakers());
        satCriticalAvgScore.setText(schoolSATScores.get(0).getSatCriticalReadingAvgScore());
        satMatAvg.setText(schoolSATScores.get(0).getSatMathAvgScore());
        satWritingAvg.setText(schoolSATScores.get(0).getSatWritingAvgScore());

    }


}
