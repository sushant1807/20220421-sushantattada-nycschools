package com.saika.nycschools.ui.school;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.saika.nycschools.AndroidApplication;
import com.saika.nycschools.Models.SchoolModel;
import com.saika.nycschools.R;
import com.saika.nycschools.repositories.AppSingletonComponent;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SchoolActivity extends AppCompatActivity implements SchoolContract.View{

    @Inject
    SchoolPresenter presenter;

    private ArrayList<SchoolModel> schoolsList = new ArrayList<>();
    private SchoolAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school);

        initPresenter();
        initAdapter();
    }

    private void initAdapter() {
        adapter = new SchoolAdapter(this, schoolsList, SchoolActivity.this);
        recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(true);

        presenter.getSchoolData();
    }

    protected AppSingletonComponent getAppSingletonComponent() {
        return ((AndroidApplication) getApplication()).getAppSingletonComponent();
    }

    private void initPresenter() {
        DaggerSchoolComponent.builder()
                .schoolPresenterModule(new SchoolPresenterModule( this, this))
                .appSingletonComponent(getAppSingletonComponent())
                .build()
                .inject(this);

    }

    @Override
    public void handleApiFailure() {
        Toast.makeText(getApplicationContext(), R.string.failure_school_message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void displaySchoolDataToView(List<SchoolModel> schoolModels) {
        adapter.replaceData(schoolModels);
    }
}
