package com.saika.nycschools.ui.school;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.saika.nycschools.Models.SchoolModel;
import com.saika.nycschools.R;
import com.saika.nycschools.Utils.Constants;
import com.saika.nycschools.ui.schooldetails.SchoolDetailsActivity;

import java.util.List;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolsViewHolder>{

    private Context context;
    private List<SchoolModel> content;
    private final Activity activity;

    public SchoolAdapter(Context context, List<SchoolModel> content, Activity activity) {
        this.context = context;
        this.content = content;
        this.activity = activity;
    }

    @NonNull
    @Override
    public SchoolAdapter.SchoolsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_school, parent, false);
        return new SchoolsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolAdapter.SchoolsViewHolder holder, int position) {
        final SchoolModel schoolModel = content.get(position);

        holder.textViewSchoolName.setText(schoolModel.getSchoolName());
        holder.textViewSchoolAddress.setText(schoolModel.getPrimaryAddressLine1());
        holder.textViewSchoolEmail.setText(schoolModel.getSchoolEmail());

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(activity, SchoolDetailsActivity.class);
            intent.putExtra(Constants.DBN, schoolModel.getDbn());
            intent.putExtra(Constants.SCHOOL_NAME, schoolModel.getSchoolName());
            intent.putExtra(Constants.PARAGRAPH, schoolModel.getOverviewParagraph());
            activity.startActivity(intent);
        });
    }

    void replaceData(List<SchoolModel> schoolModelList) {

        content.clear();
        content.addAll(schoolModelList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    public class SchoolsViewHolder extends RecyclerView.ViewHolder{
        TextView textViewSchoolName;
        TextView textViewSchoolAddress;
        TextView textViewSchoolEmail;

        public SchoolsViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewSchoolName = itemView.findViewById(R.id.school_name);
            textViewSchoolAddress = itemView.findViewById(R.id.school_address);
            textViewSchoolEmail = itemView.findViewById(R.id.school_email);
        }
    }
}
